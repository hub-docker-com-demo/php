# [php](https://hub.docker.com/_/php)

While designed for web development, the PHP scripting language also provides general-purpose use.

(Unofficial demo and howto)

## Dependency Manager: [Composer](https://getcomposer.org/)
### Official documentation
* [*How do I install Composer programmatically?*](https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md)

### Unofficial documentation
* [repology](https://repology.org/project/php/versions)
* [*How To Install and Use Composer on Ubuntu 18.04*](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04)
  2018 [Brian Hogan](https://www.digitalocean.com/community/users/bhogan)
* [docker image php composer](https://www.google.com/search?q=docker+image+php+composer)

### Run Composer as root?
* [*How do I install untrusted packages safely? Is it safe to run Composer as superuser or root?*](https://getcomposer.org/doc/faqs/how-to-install-untrusted-packages-safely.md)

#### ENV COMPOSER_ALLOW_SUPERUSER 1
* https://github.com/composer/docker
* https://hub.docker.com/_/composer/

### Docker images examples
* [shipping-docker/php-composer](https://github.com/shipping-docker/php-composer)

