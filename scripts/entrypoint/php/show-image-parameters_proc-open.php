// https://www.php.net/manual/en/function.proc-open.php
<?php
var_dump($argv);

$cmd = array_merge(['/usr/bin/env'], array_slice($argv, 1));

var_dump($cmd);

$descriptorspec = array(
   0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
   1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
   2 => array("pipe", "w")
);

$cwd = '/tmp';
$env = null;

$process = proc_open($cmd, $descriptorspec, $pipes, $cwd, $env);

var_dump($pipes);

// var_dump($_ENV);

if (is_resource($process)) {
    // $pipes now looks like this:
    // 0 => writeable handle connected to child stdin
    // 1 => readable handle connected to child stdout
    // Any error output will be appended to /tmp/error-output.txt

    fwrite($pipes[0], stream_get_contents(STDIN));
    fclose($pipes[0]);

    echo stream_get_contents($pipes[1]);
    fclose($pipes[1]);

    echo stream_get_contents($pipes[2]);
    fclose($pipes[2]);

    // It is important that you close any pipes before calling
    // proc_close in order to avoid a deadlock
    $return_value = proc_close($process);

    echo "command returned $return_value\n";

    exit($return_value);
}
?>
